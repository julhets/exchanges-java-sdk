package com.crypto.exchangeGateways.impl.binance.dtos;

import com.crypto.exchangeGateways.dtos.BaseDTO;
import com.crypto.exchangeGateways.dtos.ExchangeInfo;

import java.math.BigDecimal;

public class BinanceExchangeInfo extends BaseDTO implements ExchangeInfo {
    private final String name = "Binance";
    private final BigDecimal defaultTakerComission = new BigDecimal("0.1");
    private final BigDecimal defaultMakerComission = new BigDecimal("0.1");

    public BinanceExchangeInfo() {
    }

    public String getName() {
        return name;
    }

    public BigDecimal getDefaultTakerComission() {
        return defaultTakerComission;
    }

    public BigDecimal getDefaultMakerComission() {
        return defaultMakerComission;
    }
}
