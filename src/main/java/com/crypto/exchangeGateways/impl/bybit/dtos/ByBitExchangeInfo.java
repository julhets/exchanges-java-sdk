package com.crypto.exchangeGateways.impl.bybit.dtos;

import com.crypto.exchangeGateways.dtos.ExchangeInfo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ByBitExchangeInfo implements ExchangeInfo, Serializable {
    private final String name = "ByBit";
    private final BigDecimal defaultTakerComission = new BigDecimal("0.075");
    private final BigDecimal defaultMakerComission = new BigDecimal("-0.025");

    public ByBitExchangeInfo() {
    }

    public String getName() {
        return name;
    }

    public BigDecimal getDefaultTakerComission() {
        return defaultTakerComission;
    }

    public BigDecimal getDefaultMakerComission() {
        return defaultMakerComission;
    }
}
