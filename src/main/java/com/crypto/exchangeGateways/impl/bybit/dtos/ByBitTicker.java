package com.crypto.exchangeGateways.impl.bybit.dtos;

import com.crypto.exchangeGateways.dtos.Ticker;
import com.crypto.exchangeGateways.enums.Currency;
import com.crypto.exchangeGateways.enums.CurrencyPair;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ByBitTicker {

    @JsonProperty("symbol")
    private String symbol;

    @JsonProperty("bid_price")
    private BigDecimal bidPrice;

    @JsonProperty("ask_price")
    private BigDecimal askPrice;

    @JsonProperty("last_price")
    private BigDecimal lastPrice;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(BigDecimal bidPrice) {
        this.bidPrice = bidPrice;
    }

    public BigDecimal getAskPrice() {
        return askPrice;
    }

    public void setAskPrice(BigDecimal askPrice) {
        this.askPrice = askPrice;
    }

    public BigDecimal getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(BigDecimal lastPrice) {
        this.lastPrice = lastPrice;
    }

    public Ticker toTicker() {
        return new Ticker(
                new CurrencyPair(Currency.getInstance(getSymbol().substring(0, 3)), Currency.getInstance(getSymbol().substring(3))),
                null,
                getLastPrice(),
                getBidPrice(),
                getAskPrice(),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }
}
