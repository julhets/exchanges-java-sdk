package com.crypto.exchangeGateways.impl.binanceFutures.third.client.model.enums;

public enum  TimeInForce {
    GTC,
    IOC,
    FOK
}
