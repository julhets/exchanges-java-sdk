package com.crypto.exchangeGateways.impl.binanceFutures.third.client.impl;

import com.crypto.exchangeGateways.impl.binanceFutures.third.client.SubscriptionErrorHandler;
import com.crypto.exchangeGateways.impl.binanceFutures.third.client.SubscriptionListener;
import com.crypto.exchangeGateways.impl.binanceFutures.third.client.impl.utils.Handler;

class WebsocketRequest<T> {

    WebsocketRequest(SubscriptionListener<T> listener, SubscriptionErrorHandler errorHandler) {
        this.updateCallback = listener;
        this.errorHandler = errorHandler;
    }

    String signatureVersion = "2";
    String name;
    Handler<WebSocketConnection> connectionHandler;
    Handler<WebSocketConnection> authHandler = null;
    final SubscriptionListener<T> updateCallback;
    RestApiJsonParser<T> jsonParser;
    final SubscriptionErrorHandler errorHandler;
}
