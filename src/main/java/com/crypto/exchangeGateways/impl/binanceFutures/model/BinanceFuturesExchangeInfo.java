/*
 * *
 *  * Profitien
 *  * DISCLAIMER
 *  * Do not edit this file if you want to update this module for future new versions.
 *  *
 *  * @author Julio Reis <xtrader.inbox@protonmail.com>
 *  * @copyright Copyright (c) 2020 Profitien. (https://www.profitien.com)
 *
 */

package com.crypto.exchangeGateways.impl.binanceFutures.model;

import com.crypto.exchangeGateways.dtos.ExchangeInfo;

import java.math.BigDecimal;

public class BinanceFuturesExchangeInfo implements ExchangeInfo {
    private final String name = "Binance";
    private final BigDecimal defaultTakerComission = new BigDecimal("0.1");
    private final BigDecimal defaultMakerComission = new BigDecimal("0.1");

    public BinanceFuturesExchangeInfo() {
    }

    public String getName() {
        return "Binance";
    }

    public BigDecimal getDefaultTakerComission() {
        return this.defaultTakerComission;
    }

    public BigDecimal getDefaultMakerComission() {
        return this.defaultMakerComission;
    }
}
