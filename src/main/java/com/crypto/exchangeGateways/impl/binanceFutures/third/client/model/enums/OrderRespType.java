package com.crypto.exchangeGateways.impl.binanceFutures.third.client.model.enums;

public enum  OrderRespType {
    ACK,
    RESULT,
    FULL
}
