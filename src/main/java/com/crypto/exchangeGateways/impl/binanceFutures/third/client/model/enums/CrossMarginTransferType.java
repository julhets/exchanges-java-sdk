package com.crypto.exchangeGateways.impl.binanceFutures.third.client.model.enums;

public enum  CrossMarginTransferType {

  SUPER_MARGIN_TO_SPOT,
  SPOT_TO_SUPER_MARGIN;

}
