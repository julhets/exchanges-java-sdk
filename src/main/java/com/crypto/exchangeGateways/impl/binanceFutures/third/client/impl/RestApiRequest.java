package com.crypto.exchangeGateways.impl.binanceFutures.third.client.impl;

import okhttp3.Request;

public class RestApiRequest<T> {

  public Request request;
  RestApiJsonParser<T> jsonParser;
}
