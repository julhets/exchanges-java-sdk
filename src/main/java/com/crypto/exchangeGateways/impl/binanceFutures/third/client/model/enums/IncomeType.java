package com.crypto.exchangeGateways.impl.binanceFutures.third.client.model.enums;


public enum IncomeType {
    TRANSFER,
    WELCOME_BONUS,
    REALIZED_PNL,
    FUNDING_FEE,
    COMMISSION,
    INSURANCE_CLEAR;
}
