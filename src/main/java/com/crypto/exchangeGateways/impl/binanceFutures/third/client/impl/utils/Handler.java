package com.crypto.exchangeGateways.impl.binanceFutures.third.client.impl.utils;

@FunctionalInterface
public interface Handler<T> {

  void handle(T t);
}
