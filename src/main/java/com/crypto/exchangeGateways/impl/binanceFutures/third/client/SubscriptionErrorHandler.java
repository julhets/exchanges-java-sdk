package com.crypto.exchangeGateways.impl.binanceFutures.third.client;

import com.crypto.exchangeGateways.impl.binanceFutures.third.client.exception.BinanceApiException;

/**
 * The error handler for the subscription.
 */
@FunctionalInterface
public interface SubscriptionErrorHandler {

  void onError(BinanceApiException exception);
}
