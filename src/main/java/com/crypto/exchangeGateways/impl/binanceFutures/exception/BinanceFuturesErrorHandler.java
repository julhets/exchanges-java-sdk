package com.crypto.exchangeGateways.impl.binanceFutures.exception;

import com.crypto.exchangeGateways.exceptions.*;

public class BinanceFuturesErrorHandler implements ErrorHandler {

    //ExchangeFundsExceededException
    //ExchangeRateLimitExceededException
    //ExchangeInternalServerException
    //ExchangeSystemOverloadException
    //ExchangeInvalidCredentialsException
    //ExchangeAccountBlockedException
    //ExchangeInvalidCredentialsPermissionsException
    @Override
    public ExchangeException handleError(Exception exception) {
        String errorMessage = exception.getMessage();
        if (errorMessage.contains("API-key format invalid.")) {
            return new ExchangeInvalidCredentialsException(errorMessage);
        } else if (errorMessage.contains("Too many requests")) {
            return new ExchangeRateLimitExceededException(errorMessage);
        } else if (errorMessage.contains("Balance is insufficient.") || errorMessage.contains("Margin is insufficient.")) {
            return new ExchangeFundsExceededException(errorMessage);
        }
        return null;
    }
}
