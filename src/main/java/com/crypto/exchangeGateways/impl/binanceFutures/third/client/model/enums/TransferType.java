package com.crypto.exchangeGateways.impl.binanceFutures.third.client.model.enums;

public enum  TransferType {
    ROLL_IN,
    ROLL_OUT
}