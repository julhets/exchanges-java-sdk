package com.crypto.exchangeGateways.impl.binanceFutures.third.client.model.enums;


public enum SideEffectType {
    NO_SIDE_EFFECT,
    MARGIN_BUY,
    AUTO_REPAY;
}
