package com.crypto.exchangeGateways.impl.binanceFutures.third.client.model.enums;

public enum  WorkingType {

    MARK_PRICE,
    CONTRACT_PRICE;

}