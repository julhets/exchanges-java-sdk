package com.crypto.exchangeGateways.impl.binanceFutures.third.client.impl;

import com.crypto.exchangeGateways.impl.binanceFutures.third.client.impl.utils.JsonWrapper;

@FunctionalInterface
public interface RestApiJsonParser<T> {

  T parseJson(JsonWrapper json);
}
