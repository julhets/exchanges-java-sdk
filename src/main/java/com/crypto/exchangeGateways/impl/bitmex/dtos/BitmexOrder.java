/*
 * *
 *  * Profitien
 *  * DISCLAIMER
 *  * Do not edit this file if you want to update this module for future new versions.
 *  *
 *  * @author Julio Reis <xtrader.inbox@protonmail.com>
 *  * @copyright Copyright (c) 2020 Profitien. (https://www.profitien.com)
 *
 */

package com.crypto.exchangeGateways.impl.bitmex.dtos;

import com.crypto.exchangeGateways.dtos.BaseDTO;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.crypto.exchangeGateways.dtos.Order;
import com.crypto.exchangeGateways.enums.CurrencyPair;
import com.crypto.exchangeGateways.impl.bitmex.utils.BitmexConverterUtil;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

public class BitmexOrder extends BaseDTO {

    private final BigDecimal price;
    private final BigDecimal size;
    private final String symbol;
    private final String id;
    private final String clOrdID;
    private final Order.OrderSide side;
    private final Date timestamp;
    private final Order.OrderStatus orderStatus;
    private final String currency;
    private final String settleCurrency;

    private final String clOrdLinkID;

    private final BigDecimal simpleOrderQty;
    private final BigDecimal displayQty;
    private final BigDecimal stopPx;
    private final BigDecimal pegOffsetValue;
    private final String pegPriceType;
    private final BitmexOrderType orderType;
    private final String timeInForce;
    private final String execInst;
    private final String contingencyType;
    private final String exDestination;
    private final String triggered;
    private final boolean workingIndicator;
    private final String ordRejReason;

    private final BigDecimal simpleLeavesQty;
    private final BigDecimal leavesQty;
    private final BigDecimal simpleCumQty;
    private final BigDecimal cumQty;
    private final BigDecimal avgPx;

    private final String multiLegReportingType;
    private final String text;

    // "2018-06-03T05:22:49.018Z"
    private final Date transactTime;

    private final String error;

    public BitmexOrder(
            @JsonProperty("price") BigDecimal price,
            @JsonProperty("orderID") String id,
            @JsonProperty("orderQty") BigDecimal size,
            @JsonProperty("side") Order.OrderSide side,
            @JsonProperty("symbol") String symbol,
            @JsonProperty("clOrdID") String clOrdID,
            @JsonProperty("timestamp") Date timestamp,
            @JsonProperty("ordStatus") Order.OrderStatus orderStatus,
            @JsonProperty("currency") String currency,
            @JsonProperty("settlCurrency") String settleCurrency,
            @JsonProperty("clOrdLinkID") String clOrdLinkID,
            @JsonProperty("simpleOrderQty") BigDecimal simpleOrderQty,
            @JsonProperty("displayQty") BigDecimal displayQty,
            @JsonProperty("stopPx") BigDecimal stopPx,
            @JsonProperty("pegOffsetValue") BigDecimal pegOffsetValue,
            @JsonProperty("pegPriceType") String pegPriceType,
            @JsonProperty("ordType") BitmexOrderType orderType,
            @JsonProperty("timeInForce") String timeInForce,
            @JsonProperty("execInst") String execInst,
            @JsonProperty("contingencyType") String contingencyType,
            @JsonProperty("exDestination") String exDestination,
            @JsonProperty("triggered") String triggered,
            @JsonProperty("workingIndicator") boolean workingIndicator,
            @JsonProperty("ordRejReason") String ordRejReason,
            @JsonProperty("simpleLeavesQty") BigDecimal simpleLeavesQty,
            @JsonProperty("leavesQty") BigDecimal leavesQty,
            @JsonProperty("simpleCumQty") BigDecimal simpleCumQty,
            @JsonProperty("cumQty") BigDecimal cumQty,
            @JsonProperty("avgPx") BigDecimal avgPx,
            @JsonProperty("multiLegReportingType") String multiLegReportingType,
            @JsonProperty("text") String text,
            @JsonProperty("transactTime") Date transactTime,
            @JsonProperty("error") String error) {

        this.symbol = symbol;
        this.id = id;
        this.side = side;
        this.size = size;
        this.price = price;
        this.clOrdID = clOrdID;
        this.timestamp = timestamp;
        this.orderStatus = orderStatus;
        this.currency = currency;
        this.settleCurrency = settleCurrency;

        this.clOrdLinkID = clOrdLinkID;
        this.simpleOrderQty = simpleOrderQty;
        this.displayQty = displayQty;
        this.stopPx = stopPx;
        this.pegOffsetValue = pegOffsetValue;
        this.pegPriceType = pegPriceType;
        this.orderType = orderType;
        this.timeInForce = timeInForce;
        this.execInst = execInst;
        this.contingencyType = contingencyType;
        this.exDestination = exDestination;
        this.triggered = triggered;
        this.workingIndicator = workingIndicator;
        this.ordRejReason = ordRejReason;
        this.simpleLeavesQty = simpleLeavesQty;
        this.leavesQty = leavesQty;
        this.simpleCumQty = simpleCumQty;
        this.cumQty = cumQty;
        this.avgPx = avgPx;
        this.multiLegReportingType = multiLegReportingType;
        this.text = text;
        this.transactTime = transactTime;
        this.error = error;
    }

    public BigDecimal getPrice() {

        return price;
    }

    public BigDecimal getVolume() {

        return size;
    }

    public Order.OrderSide getSide() {

        return side;
    }

    public String getId() {

        return id;
    }

    public String getSymbol() {

        return symbol;
    }

    public Date getTimestamp() {

        return timestamp;
    }

    public Order.OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public String getCurrency() {
        return currency;
    }

    public String getSettleCurrency() {
        return settleCurrency;
    }

    public String getClOrdID() {
        return clOrdID;
    }

    public String getClOrdLinkID() {
        return clOrdLinkID;
    }

    public BigDecimal getSimpleOrderQty() {
        return simpleOrderQty;
    }

    public BigDecimal getDisplayQty() {
        return displayQty;
    }

    public BigDecimal getStopPx() {
        return stopPx;
    }

    public BigDecimal getPegOffsetValue() {
        return pegOffsetValue;
    }

    public String getPegPriceType() {
        return pegPriceType;
    }

    public BitmexOrderType getOrderType() {
        return orderType;
    }

    public String getTimeInForce() {
        return timeInForce;
    }

    public String getExecInst() {
        return execInst;
    }

    public String getContingencyType() {
        return contingencyType;
    }

    public String getExDestination() {
        return exDestination;
    }

    public String getTriggered() {
        return triggered;
    }

    public boolean isWorkingIndicator() {
        return workingIndicator;
    }

    public String getOrdRejReason() {
        return ordRejReason;
    }

    public BigDecimal getSimpleLeavesQty() {
        return simpleLeavesQty;
    }

    public BigDecimal getLeavesQty() {
        return leavesQty;
    }

    public BigDecimal getSimpleCumQty() {
        return simpleCumQty;
    }

    public BigDecimal getCumQty() {
        return cumQty;
    }

    public BigDecimal getAvgPx() {
        return avgPx;
    }

    public String getMultiLegReportingType() {
        return multiLegReportingType;
    }

    public String getText() {
        return text;
    }

    public Date getTransactTime() {
        return transactTime;
    }

    public String getError() {
        return error;
    }

    public BigDecimal getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "BitmexOrder{" +
                "price=" + price +
                ", size=" + size +
                ", symbol='" + symbol + '\'' +
                ", id='" + id + '\'' +
                ", clOrdID='" + clOrdID + '\'' +
                ", side=" + side +
                ", timestamp=" + timestamp +
                ", orderStatus=" + orderStatus +
                ", currency='" + currency + '\'' +
                ", settleCurrency='" + settleCurrency + '\'' +
                ", clOrdLinkID='" + clOrdLinkID + '\'' +
                ", simpleOrderQty=" + simpleOrderQty +
                ", displayQty=" + displayQty +
                ", stopPx=" + stopPx +
                ", pegOffsetValue=" + pegOffsetValue +
                ", pegPriceType='" + pegPriceType + '\'' +
                ", orderType=" + orderType +
                ", timeInForce='" + timeInForce + '\'' +
                ", execInst='" + execInst + '\'' +
                ", contingencyType='" + contingencyType + '\'' +
                ", exDestination='" + exDestination + '\'' +
                ", triggered='" + triggered + '\'' +
                ", workingIndicator=" + workingIndicator +
                ", ordRejReason='" + ordRejReason + '\'' +
                ", simpleLeavesQty=" + simpleLeavesQty +
                ", leavesQty=" + leavesQty +
                ", simpleCumQty=" + simpleCumQty +
                ", cumQty=" + cumQty +
                ", avgPx=" + avgPx +
                ", multiLegReportingType='" + multiLegReportingType + '\'' +
                ", text='" + text + '\'' +
                ", transactTime=" + transactTime +
                ", error='" + error + '\'' +
                '}';
    }

    public Order toOrder() {
        return new Order(
                toOrderType(orderType),
                side,
                getPrice() != null ? getPrice() : getStopPx(),
                getSize(),
                getLeavesQty(),
                BitmexCurrencyPair.toCurrencyPair(new BitmexCurrencyPair(getSymbol().substring(0, 3).toUpperCase() + "/" + getSymbol().substring(3))),
                getId(),
                getClOrdID(),
                getTimestamp(),
                getOrderStatus(),
                getExecInst(),
                getText()
        );
    }

    public static BitmexOrder build(Order order) {
        return new BitmexOrder(
                order.getPrice(),
                null,
                order.getOriginalAmount(),
                order.getOrderSide(),
                BitmexConverterUtil.toSymbol(BitmexCurrencyPair.toBitmexCurrencyPair(order.getCurrencyPair())),
                order.getUserReference(),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                order.getPrice(),
                null,
                null,
                toBitmexOrderType(order.getType()),
                null,
                null,
                null,
                null,
                null,
                false,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }

    public enum BitmexOrderType {
        Limit, LimitIfTouched, Market, MarketIfTouched, Stop, StopLimit, Unmapped;

        @JsonCreator
        public static BitmexOrderType forName(String name) {
            return Arrays.stream(values()).filter(it -> it.name().equals(name)).findFirst().orElse(BitmexOrderType.Unmapped);
        }
    }

    public static BitmexOrderType toBitmexOrderType(Order.OrderType orderType) {
        if(orderType == null) {
            return null;
        }

        if (orderType.equals(Order.OrderType.STOP_LIMIT)) {
            return BitmexOrderType.StopLimit;
        }

        return BitmexOrderType.valueOf(StringUtils.capitalize(orderType.toString().toLowerCase()));
    }

    public static Order.OrderType toOrderType(BitmexOrderType bitmexOrderType) {
        if (bitmexOrderType.equals(BitmexOrderType.StopLimit)) {
            return Order.OrderType.STOP_LIMIT;
        }

        if (bitmexOrderType.equals(BitmexOrderType.LimitIfTouched)) {
            return Order.OrderType.LIMIT;
        }

        if (bitmexOrderType.equals(BitmexOrderType.MarketIfTouched)) {
            return Order.OrderType.MARKET;
        }

        return Order.OrderType.valueOf(bitmexOrderType.toString().toUpperCase());
    }
}
