package com.crypto.exchangeGateways.impl.bitmex.dtos;

import com.crypto.exchangeGateways.dtos.BaseDTO;
import com.crypto.exchangeGateways.dtos.Order;
import com.crypto.exchangeGateways.dtos.OrderExecution;
import com.crypto.exchangeGateways.impl.bitmex.utils.BitmexCalcUtil;
import com.crypto.exchangeGateways.impl.bitmex.utils.BitmexConverterUtil;

import java.math.BigDecimal;
import java.util.Date;

public class BitmexOrderExecution extends BaseDTO {

    private String execID;
    private String orderID;
    private String clOrdID;
    private String symbol;
    private Order.OrderSide side;
    private BitmexOrder.BitmexOrderType ordType;
    private BigDecimal orderQty;
    private BigDecimal price;
    private BigDecimal leavesQty;
    private String execInst;
    private OrderExecution.ExecType execType;
    private Order.OrderStatus ordStatus;
    private BigDecimal commission;
    private BigDecimal execComm;
    private BigDecimal homeNotional;
    private Date timestamp;

    public BitmexOrderExecution() {
    }

    public BitmexOrderExecution(String execID, String orderID, String clOrdID, String symbol, Order.OrderSide side, BitmexOrder.BitmexOrderType ordType, BigDecimal orderQty, BigDecimal price, BigDecimal leavesQty, String execInst, OrderExecution.ExecType execType, Order.OrderStatus ordStatus, BigDecimal commission, BigDecimal execComm, BigDecimal homeNotional, Date timestamp) {
        this.execID = execID;
        this.orderID = orderID;
        this.clOrdID = clOrdID;
        this.symbol = symbol;
        this.side = side;
        this.ordType = ordType;
        this.orderQty = orderQty;
        this.price = price;
        this.leavesQty = leavesQty;
        this.execInst = execInst;
        this.execType = execType;
        this.ordStatus = ordStatus;
        this.commission = commission;
        this.execComm = execComm;
        this.homeNotional = homeNotional;
        this.timestamp = timestamp;
    }

    public String getExecID() {
        return execID;
    }

    public void setExecID(String execID) {
        this.execID = execID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getClOrdID() {
        return clOrdID;
    }

    public void setClOrdID(String clOrdID) {
        this.clOrdID = clOrdID;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Order.OrderSide getSide() {
        return side;
    }

    public void setSide(Order.OrderSide side) {
        this.side = side;
    }

    public BitmexOrder.BitmexOrderType getOrdType() {
        return ordType;
    }

    public void setOrdType(BitmexOrder.BitmexOrderType ordType) {
        this.ordType = ordType;
    }

    public BigDecimal getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(BigDecimal orderQty) {
        this.orderQty = orderQty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getLeavesQty() {
        return leavesQty;
    }

    public void setLeavesQty(BigDecimal leavesQty) {
        this.leavesQty = leavesQty;
    }

    public String getExecInst() {
        return execInst;
    }

    public void setExecInst(String execInst) {
        this.execInst = execInst;
    }

    public OrderExecution.ExecType getExecType() {
        return execType;
    }

    public void setExecType(OrderExecution.ExecType execType) {
        this.execType = execType;
    }

    public Order.OrderStatus getOrdStatus() {
        return ordStatus;
    }

    public void setOrdStatus(Order.OrderStatus ordStatus) {
        this.ordStatus = ordStatus;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public BigDecimal getExecComm() {
        return execComm;
    }

    public void setExecComm(BigDecimal execComm) {
        this.execComm = execComm;
    }

    public BigDecimal getHomeNotional() {
        return homeNotional;
    }

    public void setHomeNotional(BigDecimal homeNotional) {
        this.homeNotional = homeNotional;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "BitmexOrderExecution{" +
                "execID='" + execID + '\'' +
                ", orderID='" + orderID + '\'' +
                ", clOrdID='" + clOrdID + '\'' +
                ", symbol='" + symbol + '\'' +
                ", side=" + side +
                ", ordType=" + ordType +
                ", orderQty=" + orderQty +
                ", price=" + price +
                ", leavesQty=" + leavesQty +
                ", execInst='" + execInst + '\'' +
                ", execType=" + execType +
                ", ordStatus=" + ordStatus +
                ", commission=" + commission +
                ", execComm=" + execComm +
                ", homeNotional=" + homeNotional +
                ", timestamp=" + timestamp +
                '}';
    }

    public OrderExecution toOrderExecution() {
        return new OrderExecution(
                getExecID(),
                getOrderID(),
                getClOrdID(),
                BitmexCurrencyPair.toCurrencyPair(BitmexConverterUtil.toBitmexCurrencyPair(getSymbol())),
                getSide(),
                BitmexOrder.toOrderType(getOrdType()),
                getOrderQty(),
                getPrice(),
                getLeavesQty(),
                getExecInst(),
                getExecType(),
                getOrdStatus(),
                getCommission(),
                BitmexCalcUtil.convertoFromBitmexNumberFormat(getExecComm()),
                getHomeNotional(),
                getTimestamp()
        );
    }
}
