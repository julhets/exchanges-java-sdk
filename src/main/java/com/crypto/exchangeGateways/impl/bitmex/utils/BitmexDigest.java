package com.crypto.exchangeGateways.impl.bitmex.utils;

import com.crypto.exchangeGateways.utils.BaseParamsDigest;
import com.crypto.exchangeGateways.utils.CryptographyUtil;
import com.crypto.exchangeGateways.utils.NonceFactory;

import java.util.Base64;

public class BitmexDigest extends BaseParamsDigest {

    private String apiKey;

    /**
     * Constructor
     *
     * @param secretKeyBase64 the secret key to sign requests
     */
    private BitmexDigest(byte[] secretKeyBase64) {

        super(Base64.getUrlEncoder().withoutPadding().encodeToString(secretKeyBase64), HMAC_SHA_256);
    }

    private BitmexDigest(String secretKeyBase64, String apiKey) {

        super(secretKeyBase64, HMAC_SHA_256);
        this.apiKey = apiKey;
    }

    public static BitmexDigest createInstance(String secretKeyBase64) {

        if (secretKeyBase64 != null) {
            return new BitmexDigest(Base64.getUrlDecoder().decode(secretKeyBase64.getBytes()));
        }
        return null;
    }

    public static BitmexDigest createInstance(String secretKeyBase64, String apiKey) {

        return secretKeyBase64 == null ? null : new BitmexDigest(secretKeyBase64, apiKey);
    }

    public String digestParams(String httpMethod, String urlPath, String nonce, String body) {
        String payload =
                httpMethod + urlPath + nonce + body;
        return digestString(payload);
    }

    public String digestString(String payload) {
        return CryptographyUtil.bytesToHex(getMac().doFinal(payload.getBytes())).toLowerCase();
    }
}
