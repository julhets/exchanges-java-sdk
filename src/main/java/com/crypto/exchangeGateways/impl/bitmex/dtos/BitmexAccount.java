package com.crypto.exchangeGateways.impl.bitmex.dtos;

import com.crypto.exchangeGateways.dtos.Account;
import com.crypto.exchangeGateways.dtos.BaseDTO;

public class BitmexAccount extends BaseDTO {

    private String firstname;
    private String lastname;
    private String email;
    private String country;

    public BitmexAccount() {
    }

    public BitmexAccount(String firstname, String lastname, String email, String country) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.country = country;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Account toAccount() {
        return new Account(
                getFirstname() + " " + getLastname(),
                getEmail(),
                getCountry()
        );
    }
}
