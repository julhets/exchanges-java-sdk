package com.crypto.exchangeGateways.impl.bitmex.dtos;

import com.crypto.exchangeGateways.dtos.BaseDTO;
import com.crypto.exchangeGateways.dtos.Order;
import com.crypto.exchangeGateways.dtos.Trade;
import com.crypto.exchangeGateways.enums.CurrencyPair;

import java.util.Date;

public class BitmexTrade extends BaseDTO {

    private String symbol;
    private Order.OrderSide side;
    private float size;
    private float price;
    private Date timestamp;

    public BitmexTrade() {
    }

    public BitmexTrade(String symbol, Order.OrderSide side, float size, float price, Date timestamp) {
        this.symbol = symbol;
        this.side = side;
        this.size = size;
        this.price = price;
        this.timestamp = timestamp;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Order.OrderSide getSide() {
        return side;
    }

    public void setSide(Order.OrderSide side) {
        this.side = side;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "BitmexTrade{" +
                "symbol='" + symbol + '\'' +
                ", orderSide=" + side +
                ", size=" + size +
                ", price=" + price +
                ", timestamp=" + timestamp +
                '}';
    }

    public Trade toTrade() {
        return new Trade(
                BitmexCurrencyPair.toCurrencyPair(new BitmexCurrencyPair(getSymbol().substring(0, 3) + "/" + getSymbol().substring(3))),
                getSide(),
                getSize(),
                getPrice(),
                getTimestamp()
        );
    }
}
