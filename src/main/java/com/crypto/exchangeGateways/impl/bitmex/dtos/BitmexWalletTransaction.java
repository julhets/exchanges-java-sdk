package com.crypto.exchangeGateways.impl.bitmex.dtos;

import com.crypto.exchangeGateways.dtos.BaseDTO;
import com.crypto.exchangeGateways.dtos.WalletTransaction;
import com.crypto.exchangeGateways.impl.bitmex.utils.BitmexCalcUtil;
import com.crypto.exchangeGateways.impl.bitmex.utils.BitmexConverterUtil;

import java.math.BigDecimal;
import java.util.Date;

public class BitmexWalletTransaction extends BaseDTO {
    private String transactID;
    private String currency;
    private String transactType;
    private WalletTransaction.TransactionStatus transactStatus;
    private BigDecimal amount;
    private BigDecimal walletBalance;
    private Date timestamp;

    public BitmexWalletTransaction() {
    }

    public BitmexWalletTransaction(String transactID, String currency, String transactType, WalletTransaction.TransactionStatus transactStatus, BigDecimal amount, BigDecimal walletBalance, Date timestamp) {
        this.transactID = transactID;
        this.currency = currency;
        this.transactType = transactType;
        this.transactStatus = transactStatus;
        this.amount = amount;
        this.walletBalance = walletBalance;
        this.timestamp = timestamp;
    }

    public String getTransactID() {
        return transactID;
    }

    public String getCurrency() {
        return currency;
    }

    public String getTransactType() {
        return transactType;
    }

    public WalletTransaction.TransactionStatus getTransactStatus() {
        return transactStatus;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getWalletBalance() {
        return walletBalance;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "WalletTransaction{" +
                "id='" + transactID + '\'' +
                ", currencyPair=" + currency +
                ", transactionType=" + transactType +
                ", transactionStatus=" + transactStatus +
                ", amount=" + amount +
                ", walletBalance=" + walletBalance +
                ", timestamp=" + timestamp +
                '}';
    }

    public WalletTransaction toWalletTransaction() {
        return new WalletTransaction(
                getTransactID(),
                BitmexCurrency.toCurrency(BitmexConverterUtil.toBitmexCurrency(getCurrency())),
                toTransactionType(getTransactType()),
                getTransactStatus(),
                BitmexCalcUtil.convertoFromBitmexNumberFormat(getAmount()),
                BitmexCalcUtil.convertoFromBitmexNumberFormat(getWalletBalance()),
                getTimestamp()
        );
    }

    private WalletTransaction.TransactionType toTransactionType(String transactType) {
        switch (transactType) {
            case "RealisedPNL":
                return WalletTransaction.TransactionType.Realized;
            case "UnrealisedPNL":
                return WalletTransaction.TransactionType.Unrealized;
            case "Deposit":
                return WalletTransaction.TransactionType.Deposit;
            case "Transfer":
                return WalletTransaction.TransactionType.Withdrawal;
            default:
                return WalletTransaction.TransactionType.forName(transactType);
        }
    }
}
