package com.crypto.exchangeGateways.impl.bitmex.dtos;

import com.crypto.exchangeGateways.dtos.BaseDTO;
import com.crypto.exchangeGateways.dtos.ExchangeInfo;

import java.io.Serializable;
import java.math.BigDecimal;

public class BitmexExchangeInfo extends BaseDTO implements ExchangeInfo {
    private final String name = "Bitmex";
    private final BigDecimal defaultTakerComission = new BigDecimal("0.075");
    private final BigDecimal defaultMakerComission = new BigDecimal("-0.025");

    public BitmexExchangeInfo() {
    }

    public String getName() {
        return name;
    }

    public BigDecimal getDefaultTakerComission() {
        return defaultTakerComission;
    }

    public BigDecimal getDefaultMakerComission() {
        return defaultMakerComission;
    }
}
