package com.crypto.exchangeGateways.impl.bitmex.dtos;

import com.crypto.exchangeGateways.dtos.BaseDTO;
import com.crypto.exchangeGateways.dtos.Candle;
import com.crypto.exchangeGateways.enums.CandleTimeFrame;
import com.crypto.exchangeGateways.enums.CurrencyPair;

import java.util.Date;

public class BitmexCandle extends BaseDTO {

    public String symbol;
    public float open;
    public float high;
    public float low;
    public float close;
    public float volume;
    public Date timestamp;

    public BitmexCandle() {
    }

    public BitmexCandle(String symbol, float open, float high, float low, float close, float volume, Date timestamp) {
        this.symbol = symbol;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.timestamp = timestamp;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public float getOpen() {
        return open;
    }

    public void setOpen(float open) {
        this.open = open;
    }

    public float getHigh() {
        return high;
    }

    public void setHigh(float high) {
        this.high = high;
    }

    public float getLow() {
        return low;
    }

    public void setLow(float low) {
        this.low = low;
    }

    public float getClose() {
        return close;
    }

    public void setClose(float close) {
        this.close = close;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Candle{" +
                "currencyPair=" + symbol +
                ", open=" + open +
                ", high=" + high +
                ", low=" + low +
                ", close=" + close +
                ", volume=" + volume +
                ", timestamp=" + timestamp +
                '}';
    }

    public Candle toCandle(CandleTimeFrame candleTimeFrame) {
        return new Candle(
                BitmexCurrencyPair.toCurrencyPair(new BitmexCurrencyPair(getSymbol().substring(0, 3) + "/" + getSymbol().substring(3))),
                candleTimeFrame,
                getOpen(),
                getHigh(),
                getLow(),
                getClose(),
                getVolume(),
                getTimestamp()
        );
    }
}
