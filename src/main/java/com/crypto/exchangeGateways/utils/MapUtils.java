package com.crypto.exchangeGateways.utils;

import org.springframework.util.StringUtils;

import java.util.Map;

public class MapUtils {

    public static Map<String, Object> putIfNotNull(String key, Object value, Map<String, Object> map) {
        if (value != null) {
            map.put(key, value);
        }
        return map;
    }

    public static Map<String, Object> putIfNotEmpty(String key, Object value, Map<String, Object> map) {
        if (value != null && StringUtils.isEmpty(value) && !value.toString().equals("0")) {
            map.put(key, value);
        }
        return map;
    }

}
