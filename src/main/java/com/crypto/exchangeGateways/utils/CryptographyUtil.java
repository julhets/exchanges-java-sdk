/*
 * *
 *  * Profitien
 *  * DISCLAIMER
 *  * Do not edit this file if you want to update this module for future new versions.
 *  *
 *  * @author Julio Reis <xtrader.inbox@protonmail.com>
 *  * @copyright Copyright (c) 2020 Profitien. (https://www.profitien.com)
 *
 */

package com.crypto.exchangeGateways.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class CryptographyUtil {

    public static String bytesToHex(byte[] bytes) {

        char[] hexArray = "0123456789abcdef".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static byte[] calcHmacSha256(byte[] secretKey, byte[] message) {
        byte[] hmacSha256 = null;
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(message);
        } catch (Exception e) {
            throw new RuntimeException("Failed to calculate hmac-sha256", e);
        }
        return hmacSha256;
    }

}
