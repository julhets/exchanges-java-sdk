package com.crypto.exchangeGateways.dtos;

import com.crypto.exchangeGateways.enums.Currency;
import com.fasterxml.jackson.annotation.JsonCreator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

public class WalletTransaction implements Serializable {
    private final String id;
    private final Currency currency;
    private final TransactionType transactionType;
    private final TransactionStatus transactionStatus;
    private final BigDecimal amount;
    private final BigDecimal walletBalance;
    private final Date timestamp;

    public WalletTransaction(String id, Currency currency, TransactionType transactionType, TransactionStatus transactionStatus, BigDecimal amount, BigDecimal walletBalance, Date timestamp) {
        this.id = id;
        this.currency = currency;
        this.transactionType = transactionType;
        this.transactionStatus = transactionStatus;
        this.amount = amount;
        this.walletBalance = walletBalance;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getWalletBalance() {
        return walletBalance;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "WalletTransaction{" +
                "id='" + id + '\'' +
                ", currencyPair=" + currency +
                ", transactionType=" + transactionType +
                ", transactionStatus=" + transactionStatus +
                ", amount=" + amount +
                ", walletBalance=" + walletBalance +
                ", timestamp=" + timestamp +
                '}';
    }

    public enum TransactionType implements Serializable {
        Deposit,
        Withdrawal,
        Realized,
        Unrealized,
        Unmapped;

        @JsonCreator
        public static TransactionType forName(String name) {
            return Arrays.stream(values()).filter(it -> it.name().equals(name)).findFirst().orElse(TransactionType.Unmapped);
        }
    }

    public enum TransactionStatus implements Serializable {
        Pending,
        Completed,
        Canceled,
        Unmapped;

        @JsonCreator
        public static TransactionStatus forName(String name) {
            return Arrays.stream(values()).filter(it -> it.name().equals(name)).findFirst().orElse(TransactionStatus.Unmapped);
        }
    }
}
