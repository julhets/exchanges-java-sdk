package com.crypto.exchangeGateways.dtos;

import java.math.BigDecimal;

public interface ExchangeInfo {
    String getName();

    BigDecimal getDefaultTakerComission();

    BigDecimal getDefaultMakerComission();
}
