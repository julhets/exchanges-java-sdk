package com.crypto.exchangeGateways.dtos;

import com.crypto.exchangeGateways.enums.CurrencyPair;
import com.fasterxml.jackson.annotation.JsonCreator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

public class OrderExecution extends BaseDTO {

    private String execId;
    private String orderId;
    private String userReference;
    private CurrencyPair currencyPair;
    private Order.OrderSide orderSide;
    private Order.OrderType orderType;
    private BigDecimal originalAmount;
    private BigDecimal price;
    private BigDecimal leaveQty;
    private String execInst;
    private ExecType execType;
    private Order.OrderStatus status;
    private BigDecimal percCommission;
    private BigDecimal execCommission;
    private BigDecimal homeNotional;
    private Date timestamp;

    public OrderExecution() {
    }

    public OrderExecution(String execId, String orderId, String userReference, CurrencyPair currencyPair, Order.OrderSide orderSide, Order.OrderType orderType, BigDecimal originalAmount, BigDecimal price, BigDecimal leaveQty, String execInst, ExecType execType, Order.OrderStatus status, BigDecimal percCommission, BigDecimal execCommission, BigDecimal homeNotional, Date timestamp) {
        this.execId = execId;
        this.orderId = orderId;
        this.userReference = userReference;
        this.currencyPair = currencyPair;
        this.orderSide = orderSide;
        this.orderType = orderType;
        this.originalAmount = originalAmount;
        this.price = price;
        this.leaveQty = leaveQty;
        this.execInst = execInst;
        this.execType = execType;
        this.status = status;
        this.percCommission = percCommission;
        this.execCommission = execCommission;
        this.homeNotional = homeNotional;
        this.timestamp = timestamp;
    }

    public String getExecId() {
        return execId;
    }

    public void setExecId(String execId) {
        this.execId = execId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserReference() {
        return userReference;
    }

    public void setUserReference(String userReference) {
        this.userReference = userReference;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public Order.OrderSide getOrderSide() {
        return orderSide;
    }

    public void setOrderSide(Order.OrderSide orderSide) {
        this.orderSide = orderSide;
    }

    public Order.OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(Order.OrderType orderType) {
        this.orderType = orderType;
    }

    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getLeaveQty() {
        return leaveQty;
    }

    public void setLeaveQty(BigDecimal leaveQty) {
        this.leaveQty = leaveQty;
    }

    public String getExecInst() {
        return execInst;
    }

    public void setExecInst(String execInst) {
        this.execInst = execInst;
    }

    public ExecType getExecType() {
        return execType;
    }

    public void setExecType(ExecType execType) {
        this.execType = execType;
    }

    public Order.OrderStatus getStatus() {
        return status;
    }

    public void setStatus(Order.OrderStatus status) {
        this.status = status;
    }

    public BigDecimal getPercCommission() {
        return percCommission;
    }

    public void setPercCommission(BigDecimal percCommission) {
        this.percCommission = percCommission;
    }

    public BigDecimal getExecCommission() {
        return execCommission;
    }

    public void setExecCommission(BigDecimal execCommission) {
        this.execCommission = execCommission;
    }

    public BigDecimal getHomeNotional() {
        return homeNotional;
    }

    public void setHomeNotional(BigDecimal homeNotional) {
        this.homeNotional = homeNotional;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "OrderExecution{" +
                "execId='" + execId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", userReference='" + userReference + '\'' +
                ", currencyPair=" + currencyPair +
                ", orderSide=" + orderSide +
                ", orderType=" + orderType +
                ", originalAmount=" + originalAmount +
                ", price=" + price +
                ", leaveQty=" + leaveQty +
                ", execInst='" + execInst + '\'' +
                ", execType=" + execType +
                ", status=" + status +
                ", percCommission=" + percCommission +
                ", execCommission=" + execCommission +
                ", homeNotional=" + homeNotional +
                ", timestamp=" + timestamp +
                '}';
    }

    public enum ExecType implements Serializable {
        Trade, Funding, Unmapped;

        @JsonCreator
        public static ExecType forName(String name) {
            return Arrays.stream(values()).filter(it -> it.name().equals(name)).findFirst().orElse(ExecType.Unmapped);
        }
    }
}
