package com.crypto.exchangeGateways.dtos;

import com.crypto.exchangeGateways.enums.CurrencyPair;
import com.fasterxml.jackson.annotation.JsonCreator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

public class Order extends BaseDTO {
    private OrderType type;
    private OrderSide orderSide;
    private BigDecimal price;
    private BigDecimal stopPrice;
    private BigDecimal originalAmount;
    private BigDecimal leaveQty;
    private CurrencyPair currencyPair;
    private String id;
    private String userReference;
    private Date timestamp;
    private OrderStatus status;
    private String execInst;
    private String exchangeComment;
    private Map<String, Object> specificExchangeParams;

    public Order() {
    }

    public Order(OrderType type, OrderSide orderSide, BigDecimal price, BigDecimal originalAmount, BigDecimal leaveQty, CurrencyPair currencyPair, String id, String userReference, Date timestamp, OrderStatus status, String execInst) {
        this.type = type;
        this.orderSide = orderSide;
        this.price = price;
        this.originalAmount = originalAmount;
        this.leaveQty = leaveQty;
        this.currencyPair = currencyPair;
        this.id = id;
        this.userReference = userReference;
        this.timestamp = timestamp;
        this.status = status;
        this.execInst = execInst;
    }

    public Order(OrderType type, OrderSide orderSide, BigDecimal price, BigDecimal originalAmount, BigDecimal leaveQty, CurrencyPair currencyPair, String id, String userReference, Date timestamp, OrderStatus status, String execInst, String exchangeComment) {
        this.type = type;
        this.orderSide = orderSide;
        this.price = price;
        this.originalAmount = originalAmount;
        this.leaveQty = leaveQty;
        this.currencyPair = currencyPair;
        this.id = id;
        this.userReference = userReference;
        this.timestamp = timestamp;
        this.status = status;
        this.execInst = execInst;
        this.exchangeComment = exchangeComment;
    }

    public Order(OrderType type, OrderSide orderSide, BigDecimal price, BigDecimal originalAmount, BigDecimal leaveQty, CurrencyPair currencyPair, String id, String userReference, Date timestamp, OrderStatus status, String execInst, Map<String, Object> specificExchangeParams) {
        this.type = type;
        this.orderSide = orderSide;
        this.price = price;
        this.originalAmount = originalAmount;
        this.leaveQty = leaveQty;
        this.currencyPair = currencyPair;
        this.id = id;
        this.userReference = userReference;
        this.timestamp = timestamp;
        this.status = status;
        this.execInst = execInst;
        this.specificExchangeParams = specificExchangeParams;
    }

    public OrderType getType() {
        return type;
    }

    public OrderSide getOrderSide() {
        return orderSide;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public BigDecimal getLeaveQty() {
        return leaveQty;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public String getId() {
        return id;
    }

    public String getUserReference() {
        return userReference;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public String getExecInst() {
        return execInst;
    }

    public void setType(OrderType type) {
        this.type = type;
    }

    public void setOrderSide(OrderSide orderSide) {
        this.orderSide = orderSide;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
    }

    public void setLeaveQty(BigDecimal leaveQty) {
        this.leaveQty = leaveQty;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserReference(String userReference) {
        this.userReference = userReference;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public void setExecInst(String execInst) {
        this.execInst = execInst;
    }

    public String getExchangeComment() {
        return exchangeComment;
    }

    public void setExchangeComment(String exchangeComment) {
        this.exchangeComment = exchangeComment;
    }

    public Map<String, Object> getSpecificExchangeParams() {
        return specificExchangeParams;
    }

    public void setSpecificExchangeParams(Map<String, Object> specificExchangeParams) {
        this.specificExchangeParams = specificExchangeParams;
    }

    public BigDecimal getStopPrice() {
        return stopPrice;
    }

    public void setStopPrice(BigDecimal stopPrice) {
        this.stopPrice = stopPrice;
    }

    @Override
    public String toString() {
        return "Order{" +
                "type=" + type +
                ", orderSide=" + orderSide +
                ", price=" + price +
                ", originalAmount=" + originalAmount +
                ", leaveQty=" + leaveQty +
                ", currencyPair=" + currencyPair +
                ", id='" + id + '\'' +
                ", userReference='" + userReference + '\'' +
                ", timestamp=" + timestamp +
                ", status=" + status +
                ", execInst='" + execInst + '\'' +
                ", exchangeComment='" + exchangeComment + '\'' +
                ", specificExchangeParams=" + specificExchangeParams +
                '}';
    }

    public enum OrderSide implements Serializable {
        Buy, Sell, Unmapped;

        @JsonCreator
        public static OrderSide forName(String name) {
            return Arrays.stream(values()).filter(it -> it.name().equals(name)).findFirst().orElse(OrderSide.Unmapped);
        }
    }

    public enum OrderType implements Serializable {
        LIMIT, MARKET, STOP, STOP_LIMIT, UNMAPPED;

        @JsonCreator
        public static OrderType forName(String name) {
            return Arrays.stream(values()).filter(it -> it.name().equals(name)).findFirst().orElse(OrderType.UNMAPPED);
        }
    }

    public enum OrderStatus implements Serializable {
        New,
        PartiallyFilled,
        Filled,
        Cancelled,
        Rejected,
        Replaced,
        Unmapped;

        @JsonCreator
        public static OrderStatus forName(String name) {
            return Arrays.stream(values()).filter(it -> it.name().equals(name)).findFirst().orElse(OrderStatus.Unmapped);
        }
    }
}
