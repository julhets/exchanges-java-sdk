package com.crypto.exchangeGateways.dtos;

import com.crypto.exchangeGateways.enums.CurrencyPair;

import java.util.Date;

public class Trade extends BaseDTO {

    private CurrencyPair currencyPair;
    private Order.OrderSide orderSide;
    private float size;
    private float price;
    private Date timestamp;

    public Trade() {
    }

    public Trade(CurrencyPair currencyPair, Order.OrderSide orderSide, float size, float price, Date timestamp) {
        this.currencyPair = currencyPair;
        this.orderSide = orderSide;
        this.size = size;
        this.price = price;
        this.timestamp = timestamp;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public Order.OrderSide getOrderSide() {
        return orderSide;
    }

    public float getSize() {
        return size;
    }

    public float getPrice() {
        return price;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "currencyPair=" + currencyPair +
                ", orderSide=" + orderSide +
                ", size=" + size +
                ", price=" + price +
                ", timestamp=" + timestamp +
                '}';
    }
}
