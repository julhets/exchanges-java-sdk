package com.crypto.exchangeGateways.dtos;

import com.crypto.exchangeGateways.enums.CandleTimeFrame;
import com.crypto.exchangeGateways.enums.CurrencyPair;

import java.util.Date;

public class Candle extends BaseDTO {

    public CurrencyPair currencyPair;
    public CandleTimeFrame timeframe;
    public float open;
    public float high;
    public float low;
    public float close;
    public float volume;
    public Date timestamp;

    public Candle() {
    }

    public Candle(CurrencyPair currencyPair, CandleTimeFrame timeframe, float open, float high, float low, float close, float volume, Date timestamp) {
        this.currencyPair = currencyPair;
        this.timeframe = timeframe;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.timestamp = timestamp;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public CandleTimeFrame getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(CandleTimeFrame timeframe) {
        this.timeframe = timeframe;
    }

    public float getOpen() {
        return open;
    }

    public void setOpen(float open) {
        this.open = open;
    }

    public float getHigh() {
        return high;
    }

    public void setHigh(float high) {
        this.high = high;
    }

    public float getLow() {
        return low;
    }

    public void setLow(float low) {
        this.low = low;
    }

    public float getClose() {
        return close;
    }

    public void setClose(float close) {
        this.close = close;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Candle{" +
                "currencyPair=" + currencyPair +
                ", timeframe=" + timeframe +
                ", open=" + open +
                ", high=" + high +
                ", low=" + low +
                ", close=" + close +
                ", volume=" + volume +
                ", timestamp=" + timestamp +
                '}';
    }
}
