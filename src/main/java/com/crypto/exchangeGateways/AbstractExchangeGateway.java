/*
 * *
 *  * Profitien
 *  * DISCLAIMER
 *  * Do not edit this file if you want to update this module for future new versions.
 *  *
 *  * @author Julio Reis <xtrader.inbox@protonmail.com>
 *  * @copyright Copyright (c) 2020 Profitien. (https://www.profitien.com)
 *
 */

package com.crypto.exchangeGateways;

import com.crypto.exchangeGateways.exceptions.ErrorHandler;
import com.crypto.exchangeGateways.interceptors.CustomHttpRequestInterceptor;
import com.crypto.exchangeGateways.utils.ConverterUtil;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.Serializable;
import java.net.URI;
import java.util.Arrays;
import java.util.Map;

public abstract class AbstractExchangeGateway implements Serializable {
    protected final RestTemplate restTemplate = new RestTemplate();
    protected boolean isSandbox;

    protected abstract String getUrlBase();

    protected abstract ErrorHandler getErrorHandler();

    public AbstractExchangeGateway(boolean isSandbox) {
        restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
        restTemplate.setInterceptors(Arrays.asList(new CustomHttpRequestInterceptor()));

        this.isSandbox = isSandbox;
    }

    protected <T> ResponseEntity<T> get(String path, MultiValueMap<String, String> urlParams, HttpHeaders headers, ParameterizedTypeReference<T> responseType) {

        //build URL
        URI uri = UriComponentsBuilder.fromHttpUrl(getUrlBase() + path)
                .queryParams(urlParams).build().toUri();

        //build headers
        HttpHeaders privateHeaders = getHeaders("GET", uri.toString().replaceAll(getUrlBase(), ""), "");
        if (headers == null) {
            headers = privateHeaders;
        } else {
            headers.addAll(privateHeaders);
        }

        //build entity
        HttpEntity<?> entity = new HttpEntity<>(headers);

        try {
            return restTemplate.exchange(uri, HttpMethod.GET, entity, responseType);
        } catch (Exception ex) {
            throw getErrorHandler().handleError(ex);
        }
    }

    protected <T> ResponseEntity<T> post(String path, MultiValueMap<String, String> urlParams, Map<String, Object> bodyParams, HttpHeaders headers, ParameterizedTypeReference<T> responseType) {

        //build URL
        URI uri = UriComponentsBuilder.fromHttpUrl(getUrlBase() + path)
                .queryParams(urlParams).build().toUri();

        //build headers
        HttpHeaders privateHeaders = getHeaders("POST", uri.toString().replaceAll(getUrlBase(), ""), ConverterUtil.mapToJson(bodyParams));
        if (headers == null) {
            headers = privateHeaders;
        } else {
            headers.addAll(privateHeaders);
        }

        //build entity
        HttpEntity<?> entity = new HttpEntity<>(bodyParams, headers);

        try {
            return restTemplate.exchange(uri, HttpMethod.POST, entity, responseType);
        } catch (Exception ex) {
            throw getErrorHandler().handleError(ex);
        }
    }

    protected <T> ResponseEntity<T> put(String path, MultiValueMap<String, String> urlParams, Map<String, Object> bodyParams, HttpHeaders headers, ParameterizedTypeReference<T> responseType) {

        //build URL
        URI uri = UriComponentsBuilder.fromHttpUrl(getUrlBase() + path)
                .queryParams(urlParams).build().toUri();

        //build headers
        HttpHeaders privateHeaders = getHeaders("PUT", uri.toString().replaceAll(getUrlBase(), ""), ConverterUtil.mapToJson(bodyParams));
        if (headers == null) {
            headers = privateHeaders;
        } else {
            headers.addAll(privateHeaders);
        }

        //build entity
        HttpEntity<?> entity = new HttpEntity<>(bodyParams, headers);

        try {
            return restTemplate.exchange(uri, HttpMethod.PUT, entity, responseType);
        } catch (Exception ex) {
            throw getErrorHandler().handleError(ex);
        }
    }

    protected <T> ResponseEntity<T> delete(String path, MultiValueMap<String, String> urlParams, Map<String, Object> bodyParams, HttpHeaders headers, ParameterizedTypeReference<T> responseType) {

        //build URL
        URI uri = UriComponentsBuilder.fromHttpUrl(getUrlBase() + path)
                .queryParams(urlParams).build().toUri();

        //build headers
        HttpHeaders privateHeaders = getHeaders(HttpMethod.DELETE.toString(), uri.toString().replaceAll(getUrlBase(), ""), ConverterUtil.mapToJson(bodyParams));
        if (headers == null) {
            headers = privateHeaders;
        } else {
            headers.addAll(privateHeaders);
        }

        //build entity
        HttpEntity<?> entity = new HttpEntity<>(bodyParams, headers);

        try {
            return restTemplate.exchange(uri, HttpMethod.DELETE, entity, responseType);
        } catch (Exception ex) {
            throw getErrorHandler().handleError(ex);
        }
    }

    protected abstract HttpHeaders getHeaders(String method, String path, String body);
}
